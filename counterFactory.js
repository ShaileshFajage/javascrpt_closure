
function counterFactory(x) {
    
    function increament() {
        x+=5

        function decreament() {

            return x-=2;
        }

        return decreament();
    }

    return increament();
}

// let res = conuterFactory(5);

// console.log(res);

module.exports = counterFactory;