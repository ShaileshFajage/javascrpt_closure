

function limitFunctionCallCount(cb, n) {

    function x() {
        for(let i=0;i<n;i++)
        {
            cb();
        }

        if(n==0)
        {
            return null;
        }
    }

    return x();
    
}

function sayHello() {
    
    console.log("Hello");
}


module.exports = {
    limitFunctionCallCount,
    sayHello
}
